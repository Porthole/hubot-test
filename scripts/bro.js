const code = require("./bro-code.json");

module.exports = (robot) => {
  robot.hear(/bro\s?([0-9]*)?/i, (res) => {
      let index = res.match[1];
      if (index) {
        index = Number.parseInt(index);
        if (index <= code.length) {
          res.send(code[index - 1]);
        } else {
          res.send("Ta cru c'était la bible frère ?");
        }
      } else {
        res.send(res.random(code));
      }
    });


    robot.hear(/star wars (people|planet) ([0-9]+)/i, (res) => {
      const get = robot.http('https://swapi.dev/api/' + res.match[1] + '/' + res.match[2])
        .get((err, res , body) => {
          console.log(err);
          console.log(res);
          console.log(body);
        })
      res.send(typeof get);
    });
};
